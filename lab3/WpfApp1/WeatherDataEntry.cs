﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public struct WeatherDataEntry
    {
        public string City { set; get; }
        public float Temperature { set; get; }
    }
}
